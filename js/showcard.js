$(".add-to-cart").click(function (event) { 
    event.preventDefault();
    var name = $(this).attr("data-name");    
    var price = Number($(this).attr("data-price")); 
    addItemTocart(name,price,1);
    displayCard();   
});
function displayCard(){
    var cartArray = listCart();
    var output = "";
    for(var i in cartArray){
        output += 
        "<tr>"
            +"<td> "
                +"Name: "+cartArray[i].name
            +"</td> "

            +"<td> "
                +" Price: "+cartArray[i].price
            +"</td> "
            +"<td> "
                +" Totle Price:"+(cartArray[i].price*cartArray[i].count).toFixed(2)+" "
            +"</td> "

            +"<td> "
                +"<button class='Subtract-item' data-name="+cartArray[i].name+"> - </button>"
            +"</td> "

            +"<td> "
                +" Quantity: "+cartArray[i].count
            +"</td> "

            +"<td> "
                +"<button class='Pluse-item' data-name="+cartArray[i].name+"> + </button>"
            +"</td> "

            +"<td> "
                +"<button class='delet-item' data-name="+cartArray[i].name+"> <img id='clear-cart-img' src='img/close2.png'> </button>"
            +"</td> "
        +"</tr>"
    }
    $("#show-cart").html(output);
    $("#totlePrice").html(totleCartPrice());
    $("#notification").html(notificatio());
}
$("#show-cart").on("click",".delet-item", function (event){
    var name = $(this).attr("data-name");
    removeItemfromcartClearAll(name);
    displayCard();
    
})
$("#show-cart").on("click",".Subtract-item", function (event){
    var name = $(this).attr("data-name");
    removeItemFromcart(name);
    displayCard();
    
})
$("#show-cart").on("click",".Pluse-item", function (event){
    var name = $(this).attr("data-name");
    addItemTocart(name,00,1)
    displayCard();
    
})

$("#clear-cart").click(function (e) { 
    e.preventDefault();
    clearCart();
    displayCard();    
});