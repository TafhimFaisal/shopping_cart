
var cart = []; //main array to add Itme 

//creating object to strore that to cart array.every item will be stored in 
//cart array by oject every object will have the value of name price and count

var item = function (name,price,count){  
    //this function will creat object for as 
    //item and store it to the cart array by 
    //addItemTocart() Function
    this.name = name;
    this.price = price;
    this.count = count;
} 

function addItemTocart(name,price,count){

    for(var i in cart){ 
        
        if(cart[i].name === name){
            //this loop will stop repeating of adding same object in the array
            //instead if there is an entry of same object twice this increase - 
            //the value of existing object item 
            cart[i].count+= count;
            saveCart();
            return;
        }
    }

    var Item = new item(name,price,count);
    cart.push(Item);
    saveCart(); //this will save the cart in the local storage check last function written in this file
}
function notificatio(){
    var notificatio = 0;
    for(i in cart){
        notificatio++;
    }
    return notificatio;
}



function removeItemFromcart(name){
    for(var i in cart){

        if(cart[i].name === name){
            cart[i].count--;
            if(cart[i].count === 0){
                cart.splice(i,1);
            }
            break;
        }
    }
    saveCart();
}



function removeItemfromcartClearAll(name){
    for(var i in cart){
        if(cart[i].name === name){
            cart.splice(i,1);
            break;
        }
    }
    saveCart();
}

function clearCart(){
    cart=[];
    saveCart();
}

function countCart(){
    var totle=0;
    for(var i in cart){
        totle+=cart[i].count;
    }
    return totle;
}

function totleCartPrice(){
    var totleCost=0;
    for(var i in cart){
        totleCost+=cart[i].price * cart[i].count;
    }
    return totleCost.toFixed(2);
}

function listCart(){
    var cartCopy=[];

    for(var i in cart){
        var item = cart[i];
        var itemCopy = {};

        for(var p in item){
            itemCopy[p]=item[p];
        }

        cartCopy.push(itemCopy);

    }

    return cartCopy;
}

function saveCart(){
    localStorage.setItem("ShoppingCart",JSON.stringify(cart));
}

function loadCart(){
    cart = JSON.parse(localStorage.getItem("ShoppingCart"));
}

loadCart();
displayCard();
var array = listCart();
console.log(array);
console.log(notificatio());
